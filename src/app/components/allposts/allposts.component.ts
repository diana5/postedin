import { Component, OnInit, HostListener } from '@angular/core';
import { PostserviceService } from '../../services/postservice.service';
import { Post } from '../../model/post';
import { DomSanitizer } from '@angular/platform-browser';
import { faPlus, faShare } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-allposts',
    templateUrl: './allposts.component.html',
    styleUrls: ['./allposts.component.css']
})
export class AllpostsComponent implements OnInit {
    color = 'accent';
    mode = 'indeterminate';
    value = 50;
    bufferValue = 75;
    posts: Post;
    mostrar: any;
    cargando= false;
    content: boolean;
    public faPlus = faPlus;
    public faShare = faShare;
    share=false;
    public postimg="assets/post.png";

    
    //set an instance of post Service, for getting all posts by dependency injection
    constructor(
        private postService: PostserviceService,
        public sanitizer: DomSanitizer

    ) { }

    //get all posts from Postservice
    public getAllPosts() {
        this.postService.getPosts()
            .subscribe(
                result => {
                    console.log(result)
                    this.posts = <Post>result;

                }
            );

    }

  
   //show post by id veirfying id
    verifyId(id: number, idx:number) {

      //scroll depending on post
         if ( idx === 1){
          window.scrollTo(0,520)
          }

         if ( idx === 2 ){
          window.scrollTo(0,1100)
          }

         if ( idx === 3 ){
            window.scrollTo(0,1700)
          }

          if ( idx === 4){
            window.scrollTo(0,2300)
          }


          if ( idx === 5){
            window.scrollTo(0,2800)
          }

         

         
      this.content = false;
      this.cargando = true;
      this.mostrar = id;
        setTimeout(() => {
            this.cargando = false;
            this.content = true;

        }, 2000);

     return this.mostrar;  }
//function share
    public shareMedia() {
        this.share = true;
}    
   
    //when load, show all posts
    ngOnInit() {
        this.getAllPosts()
        
    }

}
