import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostserviceService {
  private postsUrl=environment.apiUrl+'/wp-json/wp/v2/posts';

  constructor(
    private http: HttpClient
  ) {}

  //Get all posts
  public getPosts(){
    return this.http.get(this.postsUrl)
 
  }

  //Get post by id
  public getPost(id:number){
    return this.http.get(this.postsUrl+'/'+id)
    .subscribe(
      result=>{
        console.log(result)
      }
    )
  }
}
