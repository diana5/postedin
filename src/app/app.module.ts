import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AllpostsComponent } from './components/allposts/allposts.component';
import { Routes } from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { KeepHtmlPipe } from './keep-html.pipe';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatButtonModule} from '@angular/material/button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { JwSocialButtonsModule } from 'jw-angular-social-buttons';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';


const appRoutes: Routes= [
  {path: 'posts', component: AllpostsComponent}, 
  {path: '', redirectTo: 'posts', pathMatch: 'full'
}
]

@NgModule({
  declarations: [
    AppComponent,
    AllpostsComponent,
    KeepHtmlPipe,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCardModule,
    HttpModule,
    HttpClientModule,
    MatProgressBarModule,
    MatButtonModule,
    FontAwesomeModule,
    JwSocialButtonsModule
    
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
