import { Title } from "@angular/platform-browser";
import { Content } from "@angular/compiler/src/render3/r3_ast";
import { Excerpt } from "./excerpt";

export class Post {
    constructor(private id: string,private date:Date, private slug: string, private excerpt:Excerpt, private title: Title, private content: Content ){}
}
