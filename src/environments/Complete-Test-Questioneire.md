 
QUESTIONNAIRE
We need to know your knowledge about to be a Frontend Developer. The idea it's to answer this questions using your project, reading directly from the url http://you-project:8000/questionnare/<question_number>.

Questions
1- Given a string, reverse each word in the sentence "Welcome to this Javascript Test!" should be become "tseT ot siht tpircsavaJ !ediuG"
var string = "Welcome to this Javascript Guide!";
<script>
var reversephrase = "";
// convert every word to array of letters
    function reverseWord(word) 
        arrayphrase = word.split("");
        //reverse word
        reversearray = arrayphrase.reverse();
        //return word string
        reverseword = reversearray.join("");
        console.log(reverseword)
        return reverseword;
    }

        //return array of words from phrase
    function splitPhrase(phrase) {
        const splitString = phrase.split(" ");
        console.log(splitString);
        splitString.forEach(element => {
            reversephrase = reversephrase + " " + reverseWord(element);;
        });

        console.log(reversephrase);
        document.write("<b>String: </b>" + phrase + "<br><b> Reverse is </b>:" + reversephrase);
    }
splitPhrase("Hello development team. How are you?")
</script>
2-Check if a given string is a palindrome "racecar" is a palindrome. "race car" should also be considered a palindrome. Case sensitivity should be taken into account
Implement enqueue and dequeue using only two stacks
<script>
    function isPalindrome(phrase) {
        //deleting white spaces
        phrase = phrase.replace(/ /g, "");
        // convert to array of letters
        arrayphrase = phrase.split("");
        reversearray = arrayphrase.reverse();
        reverseword = reversearray.join("");

        if (phrase == reverseword) {
            console.log(phrase + " Is palindrome")
            return true
        }

        if (phrase != reverseword) {
            console.log(phrase + " Is not palindrome")
            return false
        }

    }
//Examples see the console log please
    isPalindrome("casa carro auto");
    isPalindrome("reconocer");
    isPalindrome("ana");
    isPalindrome("hello friends");
    isPalindrome("race car");
</script>

3-Given an integer, determine if it is a power of 2. If so, return that number, else return -1. (0 is not a power of two)

isPowerOfTwo(4); // true
isPowerOfTwo(64); // true
isPowerOfTwo(1); // true
isPowerOfTwo(0); // false
isPowerOfTwo(-1); // false

//my code:
function isPowerOfTwo(number){
    powerof=Math.log(number)/Math.log(2) % 1;
    if(powerof===0){
        console.log(number+"is Power of 2  ")
        document.write(number+" is power of two<br>")

        return true
    }
    else{
        console.log("is not Power of 2 <br>")
        document.write("<br>"+ -1)
        return -1
    
    }
}

isPowerOfTwo(2)
isPowerOfTwo(20)
isPowerOfTwo(-2)
isPowerOfTwo(-1)
isPowerOfTwo(16)




4-Describe the functionality of the use strict; directive
"Use strict" is  used at the beginning of a code;
it is used to use javascript in a save mode, and mostly to prevent non declared variables.
this is important because we could have a bug, for example in  a case where we have many functions with the same name: 
for example
function myFunction(){
myVariable="somedata"
}

function myOtherFunction(){
myVariable="someotherdata"
}
in this example, Javascript won't know if it is the same variable, or not. Because we didn't declare it, so in this case, it will generate an error (because it is not globaly defined). 

5-What is the difference between == and === in JS?
The" ==" Operator tries to compare and convert variables even if  they are not of the same type, while the "===" means that it must be of the same type.
For example:
1=="1" //returns true, because it tries to convert number to string
while
1==="1"// return false, because it must be exactly the same type.


6-What is the difference between null and undefined
Null : it shows that a variables were defined but javascript doesn't recognize its type, maybe empty, maybe object, maybe a number, but it shows null.
Undefined: There is nothing, not defined, Javascript doesn't have anything defined inside.
This is very important for detect posible bugs, for example:
if a parameter  is passing or not.